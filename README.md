# Thememate

A quick, simple way to view theme color and typography data in-app while working on stylization.

## Table of Contents

- [Thememate](#thememate)
  - [Table of Contents](#table-of-contents)
  - [Features](#features)
  - [Getting started](#getting-started)
  - [Usage](#usage)
    - [ThememateWidget.colors](#themematewidgetcolors)
    - [ThememateWidget.typography](#themematewidgettypography)
  - [Additional Information](#additional-information)

## Features

- Display color swatches
- Display typography

## Getting started

Add the package to your `pubspec.yaml` file:

```yaml
dependencies:
    - thememate: ^0.0.2
```

or run `flutter pub add thememate`

## Usage

To create the display, create a new `ThememateDisplay`, passing the correct `ThememateWidget` enum value for the content to be shown.

### ThememateWidget.colors

```dart
const ThememateDisplay(
    thememateWidget: ThememateWidget.colors,
)
```

### ThememateWidget.typography

```dart
const ThememateDisplay(
    thememateWidget: ThememateWidget.typography,
)
```

## Additional Information

Issues and feature requests can be filed on the [official GitLab page](https://gitlab.com/singularis-technologies/packages/thememate.git)
