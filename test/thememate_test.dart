import 'package:flutter_test/flutter_test.dart';
import 'package:thememate/thememate.dart';

void main() {
  testWidgets('returns ThememateColors', (tester) async {
    ThememateDisplay thememateDisplay = const ThememateDisplay(
      thememateWidget: ThememateWidget.colors,
    );
    await tester.pumpWidget(thememateDisplay);

    final childFinder = find.descendant(
      of: find.byWidget(thememateDisplay),
      matching: find.byType(
        ThememateColors,
      ),
    );

    expect(childFinder, findsOneWidget);
  });

  testWidgets('returns ThememateTypography', (tester) async {
    ThememateDisplay thememateDisplay = const ThememateDisplay(
      thememateWidget: ThememateWidget.typography,
    );
    await tester.pumpWidget(thememateDisplay);

    final childFinder = find.descendant(
      of: find.byWidget(thememateDisplay),
      matching: find.byType(
        ThememeateTypography,
      ),
    );

    expect(childFinder, findsOneWidget);
  });
}
