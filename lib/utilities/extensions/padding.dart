import 'package:flutter/widgets.dart';

extension PaddingUtil on Widget {
  // ***************************
  // *** Left ***
  // ***************************
  Widget padLeft({required double padding}) => Padding(
        padding: EdgeInsets.only(left: padding),
        child: this,
      );

  // ***************************
  // *** Top ***
  // ***************************
  Widget padTop({required double padding}) => Padding(
        padding: EdgeInsets.only(top: padding),
        child: this,
      );

  // ***************************
  // *** Right ***
  // ***************************
  Widget padRight({required double padding}) => Padding(
        padding: EdgeInsets.only(right: padding),
        child: this,
      );

  // ***************************
  // *** Bottom ***
  // ***************************
  Widget padBottom({required double padding}) => Padding(
        padding: EdgeInsets.only(bottom: padding),
        child: this,
      );

  // ***************************
  // *** Left Right Top ***
  // ***************************
  Widget padLeftRightTop({
    required double leftAndRight,
    required double top,
  }) =>
      Padding(
        padding: EdgeInsets.only(
          left: leftAndRight,
          right: leftAndRight,
          top: top,
        ),
        child: this,
      );

  // ***************************
  // *** Left Right Bottom ***
  // ***************************
  // ignore: non_constant_identifier_names
  Widget padLeftRightBottom({
    required double leftAndRight,
    required double bottom,
  }) =>
      Padding(
        padding: EdgeInsets.only(
          left: leftAndRight,
          right: leftAndRight,
          bottom: bottom,
        ),
        child: this,
      );

  // ***************************
  // *** Symmetric ***
  // ***************************
  Widget padHorizontal({
    required double horizontal,
  }) =>
      Padding(
        padding: EdgeInsets.symmetric(
          horizontal: horizontal,
        ),
        child: this,
      );

  Widget padVertical({
    required double vertical,
  }) =>
      Padding(
        padding: EdgeInsets.symmetric(
          vertical: vertical,
        ),
        child: this,
      );

  Widget padSymmetric({
    required double horizontal,
    required double vertical,
  }) =>
      Padding(
        padding: EdgeInsets.symmetric(
          horizontal: horizontal,
          vertical: vertical,
        ),
        child: this,
      );

  // ***************************
  // *** All ***
  // ***************************

  Widget padAll({required double padding}) => Padding(
        padding: EdgeInsets.all(padding),
        child: this,
      );
}
