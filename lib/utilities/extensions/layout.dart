import 'package:flutter/widgets.dart';

extension LayoutUtil on Widget {
  Widget centered() => Center(child: this);

  Widget expanded() => Expanded(child: this);

  Widget withDirection({TextDirection textDirection = TextDirection.ltr}) =>
      Directionality(
        textDirection: textDirection,
        child: this,
      );
}
