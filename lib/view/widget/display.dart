library thememate;

import 'package:flutter/material.dart';
import 'package:thememate/model/enum/thememate_widget.dart';
import 'package:thememate/view/widget/colors.dart';
import 'package:thememate/view/widget/typography.dart';
import 'package:thememate/utilities/extensions/padding.dart';
import 'package:thememate/utilities/extensions/layout.dart';

/// A Calculator.
class ThememateDisplay extends StatelessWidget {
  final ThememateWidget thememateWidget;
  final TextDirection textDirection;

  const ThememateDisplay({
    Key? key,
    required this.thememateWidget,
    this.textDirection = TextDirection.ltr,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (thememateWidget) {
      case ThememateWidget.colors:
        return const ThememateColors()
            .padAll(padding: 8.0)
            .centered()
            .withDirection(textDirection: textDirection);
      case ThememateWidget.typography:
        return const ThememeateTypography()
            .padAll(padding: 8.0)
            .centered()
            .withDirection(textDirection: textDirection);
      default:
        return const Text('Unknown ThememateWidget enum provided.')
            .padAll(padding: 8.0)
            .centered();
    }
  }
}
