import 'package:flutter/material.dart';
import 'package:thememate/utilities/extensions/padding.dart';
import 'package:thememate/utilities/extensions/layout.dart';

class ThememateColors extends StatefulWidget {
  const ThememateColors({
    Key? key,
  }) : super(key: key);

  @override
  ThememateColorsState createState() => ThememateColorsState();
}

class ThememateColorsState extends State<ThememateColors> {
  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;

    final Map<String, Color> colorSchemeColors = {
      'OnSurface': colorScheme.onSurface,
      'Surface': colorScheme.surface,
      'OnInverseSurface': colorScheme.onInverseSurface,
      'InverseSurface': colorScheme.inverseSurface,
      'OnSurfaceVariant': colorScheme.onSurfaceVariant,
      'SurfaceVariant': colorScheme.surfaceVariant,
      'OnBackground': colorScheme.onBackground,
      'Background': colorScheme.background,
      'OnError': colorScheme.onError,
      'Error': colorScheme.error,
      'OnErrorContainer': colorScheme.onErrorContainer,
      'ErrorContainer': colorScheme.errorContainer,
      'OnPrimary': colorScheme.onPrimary,
      'Primary': colorScheme.primary,
      'OnPrimaryContainer': colorScheme.onPrimaryContainer,
      'PrimaryContainer': colorScheme.primaryContainer,
      'OnSecondary': colorScheme.onSecondary,
      'Secondary': colorScheme.secondary,
      'OnSecondaryContainer': colorScheme.onSecondaryContainer,
      'SecondaryContainer': colorScheme.secondaryContainer,
      'OnTertiary': colorScheme.onTertiary,
      'Tertiary': colorScheme.tertiary,
      'OnTertiaryContainer': colorScheme.onTertiaryContainer,
      'TertiaryContainer': colorScheme.tertiaryContainer,
      'Outline': colorScheme.outline,
      'Shadow': colorScheme.shadow,
    };

    final Map<String, Color> legacyColors = {
      'BackgroundColor': Theme.of(context).backgroundColor,
      'DialogBackgroundColor': Theme.of(context).dialogBackgroundColor,
      'ScaffoldBackgroundColor': Theme.of(context).scaffoldBackgroundColor,
      'ErrorColor': Theme.of(context).errorColor,
      'PrimaryColor': Theme.of(context).primaryColor,
      'PrimaryColorDark': Theme.of(context).primaryColorDark,
      'PrimaryColorLight': Theme.of(context).primaryColorLight,
      'BottomAppBarColor': Theme.of(context).bottomAppBarColor,
      'CanvasColor': Theme.of(context).canvasColor,
      'CardColor': Theme.of(context).cardColor,
      'DisabledColor': Theme.of(context).disabledColor,
      'DividerColor': Theme.of(context).dividerColor,
      'FocusColor': Theme.of(context).focusColor,
      'HighlightColor': Theme.of(context).highlightColor,
      'HintColor': Theme.of(context).hintColor,
      'HoverColor': Theme.of(context).hoverColor,
      'IndicatorColor': Theme.of(context).indicatorColor,
      'SecondaryHeaderColor': Theme.of(context).secondaryHeaderColor,
      'SelectedRowColor': Theme.of(context).selectedRowColor,
      'ShadowColor': Theme.of(context).shadowColor,
      'SplashColor': Theme.of(context).splashColor,
      'ToggleableActiveColor': Theme.of(context).toggleableActiveColor,
      'UnselectedWidgetColor': Theme.of(context).unselectedWidgetColor,
    };

    Widget buildListItem(dynamic key, dynamic value) {
      return Card(
        child: Column(
          children: [
            Container(
              width: 170,
              height: 170,
              decoration: BoxDecoration(
                color: value,
                shape: BoxShape.circle,
              ),
            ),
            Text(
              key,
              style: Theme.of(context).textTheme.caption,
            ).padTop(padding: 8.0),
          ],
        ).padAll(padding: 16.0).centered(),
      );
    }

    List<Widget> buildColorList(String listName) {
      List<Widget> list = <Widget>[];

      switch (listName) {
        case 'colorSchemeColors':
          colorSchemeColors.forEach((key, value) {
            list.add(buildListItem(key, value));
          });
          break;
        case 'legacyColors':
          legacyColors.forEach((key, value) {
            list.add(buildListItem(key, value));
          });
          break;
        default:
          break;
      }

      return list;
    }

    return Column(
      children: [
        Text(
          'ColorScheme',
          style: Theme.of(context).textTheme.headline4,
        ).padBottom(padding: 8.0),
        GridView.count(
          crossAxisCount: 2,
          crossAxisSpacing: 4,
          mainAxisSpacing: 4,
          children: buildColorList('colorSchemeColors'),
        ).expanded(),
        Text(
          'Legacy',
          style: Theme.of(context).textTheme.headline4,
        ).padBottom(padding: 8.0),
        GridView.count(
          crossAxisCount: 2,
          crossAxisSpacing: 4,
          mainAxisSpacing: 4,
          children: buildColorList('legacyColors'),
        ).expanded(),
      ],
    );
  }
}
