import 'package:flutter/material.dart';
import 'package:thememate/utilities/extensions/padding.dart';
import 'package:thememate/utilities/extensions/layout.dart';

class ThememeateTypography extends StatefulWidget {
  const ThememeateTypography({
    Key? key,
  }) : super(key: key);

  @override
  ThememeateTypographyState createState() => ThememeateTypographyState();
}

class ThememeateTypographyState extends State<ThememeateTypography> {
  @override
  Widget build(BuildContext context) {
    final Map<String, TextStyle?> textStyles = {
      'Headline 1': Theme.of(context).textTheme.headline1,
      'Headline 2': Theme.of(context).textTheme.headline2,
      'Headline 3': Theme.of(context).textTheme.headline3,
      'Headline 4': Theme.of(context).textTheme.headline4,
      'Headline 5': Theme.of(context).textTheme.headline5,
      'Headline 6': Theme.of(context).textTheme.headline6,
      'Subtitle 1': Theme.of(context).textTheme.subtitle1,
      'Subtitle 2': Theme.of(context).textTheme.subtitle2,
      'Body Text 1': Theme.of(context).textTheme.bodyText1,
      'Body Text 2': Theme.of(context).textTheme.bodyText2,
      'Button': Theme.of(context).textTheme.button,
      'Caption': Theme.of(context).textTheme.caption,
      'Overline': Theme.of(context).textTheme.overline,
    };

    return Column(
      children: [
        ListView.builder(
          itemCount: textStyles.length,
          itemBuilder: (BuildContext context, int index) {
            return Card(
              child: Text(
                textStyles.keys.elementAt(index),
                style: textStyles.values.elementAt(index),
              ).padAll(padding: 8.0).centered(),
            ).padSymmetric(horizontal: 8.0, vertical: 4.0);
          },
        ).expanded(),
      ],
    );
  }
}
